import nuke

nuke.menu('Nodes').addMenu('MW')
nuke.menu('Nodes').addCommand('MW/EBSynth', lambda: nuke.createNode('EBSynth'))

nuke.load('EBSynth')