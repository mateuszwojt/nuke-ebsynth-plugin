// Copyright Mateusz Wojt, 2021. All rights reserved.

#ifdef _WIN32
#pragma warning(disable: 4996)
#endif

#include <DDImage/LUT.h>

#include "NukeEBSynth.h"

#include "ebsynth/ebsynth.h"

using namespace DD::Image;

EBSynth::EBSynth(Node* node) : PlanarIop(node)
{
	m_weight = 5;
	m_uniformityWeight = 3500;
	m_patchSize = 5;
	m_numPyramidLevels = -1;
	m_numSearchVoteIters = 6;
	m_numPatchMatchIters = 4;
	m_stopThreshold = 3;
	m_extraPass3x3 = false;
	m_backend = EBSYNTH_BACKEND_CPU;  // hardcode CPU for now...

	m_defaultChannels = Mask_RGBA;
	m_defaultNumberOfChannels = m_defaultChannels.size();

};

void EBSynth::knobs(Knob_Callback f)
{
	Float_knob(f, &m_weight, "weight", "Weight");
	Float_knob(f, &m_uniformityWeight, "uniformityWeight", "Uniformity Weight");

	Int_knob(f, &m_patchSize, "patchSize", "Patch Size");
	Int_knob(f, &m_numPyramidLevels, "numPyramidLevels", "Number of Pyramid Levels");
	Int_knob(f, &m_numSearchVoteIters, "numSearchVoteIters", "Search Vote Iters Number");
	Int_knob(f, &m_numPatchMatchIters, "numPatchMatchIters", "Patch Match Iters Number");
	Int_knob(f, &m_stopThreshold, "stopThreshold", "Stop Treshold");
	Bool_knob(f, &m_extraPass3x3, "extraPass3x3", "Extra Filtering Pass");
}

const char* EBSynth::input_label(int n, char*) const
{
	switch (n) {
	case 0:
		return "src";
	case 1:
		return "srcGuides";
	case 2:
		return "tgtGuides";
	default:
		return 0;
	}
}

void EBSynth::_validate(bool for_real)
{
// basic validation
	copy_info();
	merge_info();
}

void EBSynth::getRequests(const Box & box, const ChannelSet & channels, int count, RequestOutput & reqData) const
{
	for (int i = 0, endI = getInputs().size(); i < endI; i++) 
	{
		const ChannelSet readChannels = input(i)->info().channels();
		input(i)->request(readChannels, count);
	}
}

void EBSynth::renderStripe(ImagePlane &plane)
{
	if (aborted() || cancelled())
		return;

	const Box imageFormat = info().format();
	auto sourceWidth = imageFormat.w();
	auto sourceHeight = imageFormat.h();

	// Initialize buffers
	std::vector<unsigned char> sourceStyle(sourceWidth*sourceHeight*m_defaultNumberOfChannels);
	std::vector<unsigned char> sourceGuides(sourceWidth*sourceHeight*m_defaultNumberOfChannels);
	std::vector<unsigned char> targetGuides(sourceWidth*sourceHeight*m_defaultNumberOfChannels);

	// Iterate over inputs
	for (auto i = 0; i < node_inputs(); ++i) 
	{
		if (aborted() || cancelled())
			return;

		Iop* inputIop = dynamic_cast<Iop*>(input(i));

		if (inputIop == nullptr) {
			continue;
		}

		// Validate input just in case before further processing.
		if (!inputIop->tryValidate(true)) {
			continue;
		}

		// Set our input bounding box, this is what our inputs can give us.
		Box imageBounds = inputIop->info();

		// We're going to clip it to our format.
		imageBounds.intersect(imageFormat);
		const int fx = imageBounds.x();
		const int fy = imageBounds.y();
		const int fr = imageBounds.r();
		const int ft = imageBounds.t();

		// Request input based on our format.
		inputIop->request(fx, fy, fr, ft, m_defaultChannels, 0);

		// Fetch plane from input into the image plane.
		ImagePlane inputPlane(imageBounds, false, m_defaultChannels, m_defaultNumberOfChannels);
		inputIop->fetchPlane(inputPlane);

		auto chanStride = inputPlane.chanStride();
		auto numPixels = inputPlane.bounds().area();

		// Iterate over each channel and get pixel values.
		for (auto chanNo = 0; chanNo < m_defaultNumberOfChannels; chanNo++)
		{
			const float* indata = &inputPlane.readable()[chanStride * chanNo];

			for (auto x = 0; x < fr; x++)
			{
				for (auto y = 0; y < ft; y++) {
					if (i == 0)
						sourceStyle[(y * fr + x) * m_defaultNumberOfChannels + chanNo] = indata[y * fr + x];
					if (i == 1)
						sourceGuides[(y * fr + x) * m_defaultNumberOfChannels + chanNo] = indata[y * fr + x];
					if (i == 2)
						targetGuides[(y * fr + x) * m_defaultNumberOfChannels + chanNo] = indata[y * fr + x];
				}
			}
		}
	}

	std::vector<float> styleWeights(m_defaultNumberOfChannels);  // TODO: should be number of style channels
	for (auto i = 0; i < m_defaultNumberOfChannels; i++)
	{
		styleWeights[i] = m_weight / float(m_defaultNumberOfChannels);;
	}

	std::vector<float> guideWeights(m_defaultNumberOfChannels);  // TODO: should be number of guide channels
	{
		int c = 0;
		
		for(int j=0;j<m_defaultNumberOfChannels;j++)
		{
			guideWeights[c+j] = m_weight / float(m_defaultNumberOfChannels);
		}

		c += m_defaultNumberOfChannels;
  	}

	std::vector<int> numSearchVoteItersPerLevel(m_numPyramidLevels);
	std::vector<int> numPatchMatchItersPerLevel(m_numPyramidLevels);
	std::vector<int> stopThresholdPerLevel(m_numPyramidLevels);
	
	for (auto i = 0; i < m_numPyramidLevels;i++)
	{
		numSearchVoteItersPerLevel[i] = m_numSearchVoteIters;
		numPatchMatchItersPerLevel[i] = m_numPatchMatchIters;
		stopThresholdPerLevel[i] = m_stopThreshold;
	}

	ebsynthRun(m_backend,
			m_defaultNumberOfChannels,  // number of source channels
			m_defaultNumberOfChannels,  // number of guide channels, TODO: fetch actual number of channels from the input
			sourceWidth,  // source width
			sourceHeight,  // source height
			sourceStyle.data(),
			sourceGuides.data(),
			sourceWidth,  // target width, assume for now it's matching the source size
			sourceHeight,  // target height, assume for now it's matching the source size
			targetGuides.data(),
			nullptr,
			styleWeights.data(),
			guideWeights.data(),
			m_uniformityWeight,
			m_patchSize,
			EBSYNTH_VOTEMODE_PLAIN,
			m_numPyramidLevels,
			numSearchVoteItersPerLevel.data(),
			numPatchMatchItersPerLevel.data(),
			stopThresholdPerLevel.data(),
			m_extraPass3x3,
			nullptr,
			m_output.data());

	float* dest = plane.writable();
	const ChannelSet channels = plane.channels();

	foreach(z, channels)
	{
		const uint8_t chanOffset = colourIndex(z);
		Linear::from_float(
			dest + sourceWidth * sourceHeight * chanOffset,
			reinterpret_cast<float*>(m_output.data()) + chanOffset,
			sourceWidth * sourceHeight,
			m_defaultNumberOfChannels);
	}
}

static Iop* build(Node* node) { return new EBSynth(node); }
const Iop::Description EBSynth::d("EBSynth", "Other/EBSynth", build);