// Copyright Mateusz Wojt, 2022. All rights reserved.

#include <DDImage/PlanarIop.h>
#include <DDImage/Knobs.h>
#include <DDImage/Knob.h>

static const char* const HELP = "Plugin based on fast example-based image synthesizer called EBSynth.";
static const char* const CLASS = "EBSynth";

using namespace DD::Image;

class EBSynth : public PlanarIop
{
public:
// constructor
    EBSynth(Node* node);

// Nuke internal methods
	int minimum_inputs() const { return 3; }
	int maximum_inputs() const { return 3; }

	PackedPreference packedPreference() const { return ePackedPreferenceUnpacked; }

	void knobs(Knob_Callback f);

	void _validate(bool);

	void getRequests(const Box& box, const ChannelSet& channels, int count, RequestOutput &reqData) const;
	virtual void renderStripe(ImagePlane& plane);

	bool useStripes() const { return false; }

	bool renderFullPlanes() const { return true; }

	const char* input_label(int n, char*) const;
    static const Iop::Description d;

	const char* Class() const { return CLASS; }
	const char* node_help() const { return HELP; }

// private class members
private:
	float						m_weight;
	float						m_uniformityWeight;

	int							m_patchSize;
	int							m_numPyramidLevels;
	int							m_numSearchVoteIters;
	int							m_numPatchMatchIters;
	int							m_stopThreshold;
	bool						m_extraPass3x3;
	int							m_backend;

	ChannelSet					m_defaultChannels;
	int 						m_defaultNumberOfChannels;

	std::vector<unsigned char>	m_output;
};